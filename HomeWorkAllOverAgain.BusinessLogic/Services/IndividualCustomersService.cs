﻿using HomeWorkAllOverAgain.BusinessLogic.Models;
using HomeWorkAllOverAgain.DataLogic;
using HomeWorkAllOverAgain.DataLogic.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.BusinessLogic.Services
{
    public class IndividualCustomersService
    {
        private IDataObjectMapper _dataObjectsMapper = new DataObjectMapper();
        IndividualCustomersService _individualCustomersService = new IndividualCustomersService();

        public void AddCustomer(IndividualCustomerBl individualCustomerBl)
        {
            if(_individualCustomersService.IsLoginUsed(individualCustomerBl.login))
            {
                throw new Exception("Entered login is used by other user, please Enter other login");
            }
            using (var dbContext = new HomeWorkAllOverAgainDbContext())
            {
                dbContext.IndividualCustomersDbSet.Add(_dataObjectsMapper.MapIndividualCustomerBlToIndividualCustomer(individualCustomerBl));
                dbContext.SaveChanges();
            }
        }

        public List<TransactionBl> GetTransactions(string login)
        {
            var customer = _individualCustomersService.GetCustomer(login);
            var customerBl = _dataObjectsMapper.MapIndividualCustomerToIndividualCustomerBl(customer);

            return customerBl.transactions;
        }

        public void RecordTransaction(string login, TransactionBl transactionBl)
        {
            var customer = _individualCustomersService.GetCustomer(login);
            var customerBl = _dataObjectsMapper.MapIndividualCustomerToIndividualCustomerBl(customer);

            customerBl.transactions.Add(transactionBl);

            _individualCustomersService.UpdateCustomer(_dataObjectsMapper.MapIndividualCustomerBlToIndividualCustomer(customerBl));
        }

        public bool IsLoginUsed(string login)
        {
            using (var dbContext = new HomeWorkAllOverAgainDbContext())
            {
                return dbContext.IndividualCustomersDbSet.Count(X => X.login == login) > 0;
            }
        }

        public void UpdateCustomer(IndividualCustomer individualCustomer)
        {
            using (var dbContext = new HomeWorkAllOverAgainDbContext())
            {
                dbContext.IndividualCustomersDbSet.Attach(individualCustomer);
                dbContext.Entry(individualCustomer).State = EntityState.Modified;

                dbContext.SaveChanges();
            }
        }

        public IndividualCustomer GetCustomer(string login)
        {
            using (var dbContext = new HomeWorkAllOverAgainDbContext())
            {
                return dbContext.IndividualCustomersDbSet.SingleOrDefault(x => x.login == login);
            }
        }
    }
}
