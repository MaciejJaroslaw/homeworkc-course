﻿using HomeWorkAllOverAgain.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.BusinessLogic.Models
{
    public class IndividualCustomerBl
    {
        public string login;
        public string password;
        public string name;
        public string surname;
        public string age;
        public int Id;

        public List<TransactionBl> transactions = new List<TransactionBl>();
    }
}
