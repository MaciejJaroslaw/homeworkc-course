﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWorkAllOverAgain;
using HomeWorkAllOverAgain.BusinessLogic;
using HomeWorkAllOverAgain.BusinessLogic.Models;
using HomeWorkAllOverAgain.BusinessLogic.Services;
using HomeWorkAllOverAgain.Helpers;

namespace HomeWorkAllOverAgain
{
    class Program
    {
        private IEventsService _eventsService = new EventsService();
        private Menu _menu = new Menu();
        private bool quit;
        private DataObjectMapper _dataObjectMapper = new DataObjectMapper();

        Program()
        {
            IEventsService _eventsService = new EventsService();
            Menu _menu = new Menu();
        }

        public static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            _menu.AddComand("Exit", Exit);
            _menu.AddComand("Create new event", CreateEvent);
            _menu.AddComand("See already events", ShowAllEvents);
            _menu.AddComand("Order new tickets", OrderNewTickets);
            _menu.AddComand("See all details of event", SeeAllDetails);

            while (!quit)
            {
                _menu.PrintAllComands();
                var command = IoHelper.GetStringFromUser("Select command");
                _menu.RunCommand(command);
            }
        }

        public void Exit()
        {
            quit = true;
        }

        public void CreateEvent()
        {
            var newEvent = new EventBl
            {
                organizer = IoHelper.GetStringFromUser("Who is the organizer?"),
                name = IoHelper.GetStringFromUser("Enter name for this event"),
                date = IoHelper.GetDateTimeFromUser("Enter the date: dd-mm-yyyy"),
                description = IoHelper.GetStringFromUser("Give me description of this event")
            };

            var ticketsList = new List<TicketBl>();

            Console.WriteLine("How many kind of tickets you want create?");
            var amountOfTickets = int.Parse(Console.ReadLine());

            for (int i = 0; i < amountOfTickets; i++)
            {
                var newTicket = new TicketBl
                {
                    kind = IoHelper.GetStringFromUser("Enter new name for Kind of ticket"),
                    price = IoHelper.GetIntFromUser("give price of this kind"),
                    initialAmount = IoHelper.GetIntFromUser("Give me initial amount of tickets"),
                };
                newTicket.leftAmount = newTicket.initialAmount;
                ticketsList.Add(newTicket);
            }
            newEvent.totalAmount = ticketsList.Sum(x => x.initialAmount);
            newEvent.totalLeftAmount = newEvent.totalAmount;

            _eventsService.AddEvent(newEvent, ticketsList);
        }

        public void ShowAllEvents()
        {
            List<EventBl> listOfEvents = _eventsService.GetAllEvents();

            foreach (EventBl eventbl in listOfEvents)
            {
                Console.WriteLine($"name of event:{eventbl.name}");
                Console.WriteLine($"date of event: {eventbl.date}");
                Console.WriteLine($"description of event: {eventbl.description}");
                Console.WriteLine("\n");
            }
        }

        private void OrderNewTickets()
        {
            Console.WriteLine("List of available events:");
            var events = _eventsService.GetAllEvents();
            int j = 0;
            foreach (EventBl eventBl in events)
            {
                j++;
                Console.WriteLine($"[{j}] { eventBl.name}");
            }

            Console.WriteLine("select the event from list");
            int selectedNumber = int.Parse(Console.ReadLine());
            var chosenEvent = events[selectedNumber - 1];
            int ticketId;

            int amountOfTickets = IoHelper.GetIntFromUser("How many tickets you want buy?");
            for (int i = 0; i < amountOfTickets; i++)
            {
                var newPurchaser = new PurchaserBl();
                do
                {
                    Console.WriteLine("available kind of ticket");
                    int z = 0;
                    foreach (TicketBl ticketBl in chosenEvent.tickets)
                    {
                        z++;
                        Console.WriteLine($"[{z}] tickiet {ticketBl.kind} for {ticketBl.price} pln");
                    }
                    Console.WriteLine("select kind of ticket");
                    ticketId = int.Parse(Console.ReadLine());

                    if (!_eventsService.checkAvailability(chosenEvent.uid, chosenEvent.tickets[ticketId - 1].kind))
                    {
                        Console.WriteLine("We have not enough this kind of ticket, you should to selected other kind");
                    }
                }
                while (ticketId == -1);

                newPurchaser.kindOfTicket = chosenEvent.tickets[ticketId - 1].kind;
                newPurchaser.name = IoHelper.GetStringFromUser("Enter the name of purchaser");
                newPurchaser.surname = IoHelper.GetStringFromUser("Enter the surname of purchaser");
                newPurchaser.age = IoHelper.GetIntFromUser("Enter the age of purchaser");

                _eventsService.OrderTicket(chosenEvent.uid, newPurchaser);
            }
        }

        public void SeeAllDetails()
        {
            Console.WriteLine("Select event's index to see all details");
            var eventsList = _eventsService.GetAllEvents();
            int x = 0;
            foreach (EventBl eventBl in eventsList)
            {
                x++;
                Console.WriteLine($"{x} {eventBl.name}");
            }

            var selectedEvent = int.Parse(Console.ReadLine());
            var oneEvent = eventsList[selectedEvent - 1];

            Console.WriteLine($"organizer: {oneEvent.organizer}");
            Console.WriteLine($"name: {oneEvent.name}");
            Console.WriteLine($"date: {oneEvent.date}");
            Console.WriteLine($"description: {oneEvent.description}");
            Console.WriteLine($"amount of left tickets: {oneEvent.totalLeftAmount}");

            foreach (TicketBl ticketBl in oneEvent.tickets)
            {
                Console.WriteLine($"kind of ticket: {ticketBl.kind}");
                Console.WriteLine($"price of ticket: {ticketBl.price}");
                Console.WriteLine($"amount of left tickets: {ticketBl.leftAmount}");
                Console.WriteLine("\n");
            }

            foreach (PurchaserBl purchaserBl in oneEvent.purchasers)
            {
                Console.WriteLine($"Name of Purchaser: {purchaserBl.name}");
                Console.WriteLine($"Surname of Purchaser: {purchaserBl.surname}");
                Console.WriteLine($"Kind of purchased ticket: {purchaserBl.kindOfTicket}");
                Console.WriteLine($"age of purchaser: {purchaserBl.age}");
            }

            var stats = _eventsService.GetStatistic(oneEvent.uid);

            Console.WriteLine($": {stats.global.HighestAge}");
            Console.WriteLine($": {stats.global.LowestAge}");
            Console.WriteLine($": {stats.global.MediumAge}");
            Console.WriteLine($": {stats.global.Benefit}");

            foreach (var statInd in stats.ticketsStatistic)
            {
                Console.WriteLine($"Statistics for: {statInd.Key}");
                Console.WriteLine($": {statInd.Value.HighestAge}");
                Console.WriteLine($": {statInd.Value.LowestAge}");
                Console.WriteLine($": {statInd.Value.MediumAge}");
                Console.WriteLine($": {statInd.Value.Benefit}");
            }

            Console.WriteLine("Do you want to export theese data to file in Json format? Yes/No");
            string String = Console.ReadLine();
            if (String == "Yes")
            {
                Console.WriteLine("Enter the file name:");
                var NameOfFile = Console.ReadLine();
                new JsonFileManager().SaveToFile(NameOfFile, _dataObjectMapper.MapEventBlToEvent(oneEvent), stats);
            }
            else
            {
                _menu.PrintAllComands();
                var command = IoHelper.GetStringFromUser("Select command");
                _menu.RunCommand(command);
            }

        }
    }
}
        
