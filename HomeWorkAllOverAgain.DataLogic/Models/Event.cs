﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.DataLogic.Models
{
    public class Event
    {
        public string Organizer { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public int TotalLeftAmount { get; set; }
        public int TotalAmount { get; set; }
        public int Id { get; set; }

        public List<Purchaser> purchasers { get; set; } = new List<Purchaser>();
        public List<Ticket> tickets { get; set; } = new List<Ticket>();
    }
}
