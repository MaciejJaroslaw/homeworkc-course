﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.DataLogic.Models
{
    public class Ticket
    {
        public string kind { get; set; }
        public int price { get; set; }
        public int initialAmount { get; set; }
        public int leftAmount { get; set; }
        public int Id { get; set; }
    }
}
