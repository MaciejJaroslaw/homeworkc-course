﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.Entity;
using System.Threading.Tasks;
using HomeWorkAllOverAgain.DataLogic.Models;

namespace HomeWorkAllOverAgain.DataLogic
{
    public class HomeWorkAllOverAgainDbContext : DbContext
    {
        public HomeWorkAllOverAgainDbContext() : base(GetConnectionString())
        { }

        public DbSet<Event> EventsDbSet { get; set; }
        public DbSet<IndividualCustomer> IndividualCustomersDbSet { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["HomeWorkAllOverAgainDbConnectionString"].ConnectionString;
        }
    }
}
